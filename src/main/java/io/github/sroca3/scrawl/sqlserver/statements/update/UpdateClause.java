package io.github.sroca3.scrawl.sqlserver.statements.update;

import io.github.sroca3.scrawl.sqlserver.statements.common.AssignmentExpression;
import io.github.sroca3.scrawl.sqlserver.statements.update.SetClause;

public interface UpdateClause {

    SetClause set(AssignmentExpression... assignmentExpressions);
}
