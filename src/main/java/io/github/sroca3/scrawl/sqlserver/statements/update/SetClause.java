package io.github.sroca3.scrawl.sqlserver.statements.update;

import io.github.sroca3.scrawl.sqlserver.statements.common.TerminatingClause;
import io.github.sroca3.scrawl.sqlserver.schema.Condition;

public interface SetClause extends TerminatingClause {

    TerminatingClause where(Condition condition);
}
