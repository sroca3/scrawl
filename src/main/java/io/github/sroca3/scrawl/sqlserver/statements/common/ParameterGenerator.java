package io.github.sroca3.scrawl.sqlserver.statements.common;

public interface ParameterGenerator {
    void generateParameters(Parameters parameters);
}
