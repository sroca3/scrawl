package io.github.sroca3.scrawl.sqlserver.statements.select;

import io.github.sroca3.scrawl.sqlserver.schema.Table;
import io.github.sroca3.scrawl.sqlserver.statements.common.FromClause;
import io.github.sroca3.scrawl.sqlserver.statements.select.SelectStatementFromClause;

public interface SelectColumnsClause {

    SelectStatementFromClause from(String tableName);

    SelectStatementFromClause from(Table<?> table);
}
