package io.github.sroca3.scrawl.sqlserver.statements.common;

import io.github.sroca3.scrawl.sqlserver.schema.Column;

public interface HavingClause extends TerminatingClause {
    TerminatingClause orderBy(Column... columns);
}
