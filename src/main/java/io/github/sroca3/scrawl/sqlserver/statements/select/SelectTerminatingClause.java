package io.github.sroca3.scrawl.sqlserver.statements.select;

import io.github.sroca3.scrawl.sqlserver.statements.common.TerminatingClause;

public interface SelectTerminatingClause extends TerminatingClause {

    SelectTerminatingClause union(SelectTerminatingClause selectTerminatingClause);
}
