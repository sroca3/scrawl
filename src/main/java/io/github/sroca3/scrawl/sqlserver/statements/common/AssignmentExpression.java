package io.github.sroca3.scrawl.sqlserver.statements.common;

import io.github.sroca3.scrawl.sqlserver.schema.SqlHolder;

public interface AssignmentExpression extends ParameterGenerator, SqlHolder {
}
