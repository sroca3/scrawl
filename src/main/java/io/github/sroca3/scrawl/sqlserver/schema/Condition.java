package io.github.sroca3.scrawl.sqlserver.schema;

import io.github.sroca3.scrawl.sqlserver.statements.common.ParameterGenerator;

public interface Condition extends SqlHolder, ParameterGenerator {
    Condition and(Condition condition);

    Condition or(Condition condition);
}
