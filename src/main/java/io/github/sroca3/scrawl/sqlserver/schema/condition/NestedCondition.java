package io.github.sroca3.scrawl.sqlserver.schema.condition;

import io.github.sroca3.scrawl.sqlserver.schema.Condition;

public class NestedCondition extends CombiningCondition implements Condition {

    public NestedCondition(Condition leftCondition, String logicalOperator, Condition rightCondition) {
        super(leftCondition, logicalOperator, rightCondition);
    }

    @Override
    public String getSql() {
        String sql = this.leftCondition.getSql();
        sql += SPACE + logicalOperator + SPACE;
        sql += "(" + this.rightCondition.getSql() + ")";
        return sql;
    }
}
