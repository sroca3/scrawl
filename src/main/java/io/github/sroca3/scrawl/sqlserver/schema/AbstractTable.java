package io.github.sroca3.scrawl.sqlserver.schema;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

public abstract class AbstractTable<T extends AbstractTable<T>> implements Table<T> {

    protected final String schemaName;
    protected final String tableName;
    protected String alias;
    protected Map<String, Column> columns = new LinkedHashMap<>();

    public AbstractTable(String tableName) {
        this(tableName, null);
    }

    public AbstractTable(String tableName, String alias) {
        this(null, tableName, alias);
    }

    public AbstractTable(String schemaName, String tableName, String alias) {
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.alias = alias;
    }

    @Override
    public String getName() {
        if (schemaName != null) {
            return String.join(".", schemaName, tableName);
        }
        return tableName;
    }

    @Override
    public String getAlias() {
        return alias;
    }

    protected Column column(String columnName) {
        return Optional.ofNullable(columns.get(columnName))
            .orElseGet(() -> {
                    var column = Optional.ofNullable(getAlias())
                        .map(alias -> new SimpleColumn(String.join(".", alias, columnName)))
                        .orElse(new SimpleColumn(columnName));
                    this.columns.put(columnName, column);
                    return column;
                }
            );
    }

    public Column[] columns() {
        return columns.values().toArray(new Column[0]);
    }

    @Override
    public T as(String alias) {
        try {
            var constructor = getSubClass().getDeclaredConstructor();
            constructor.setAccessible(true);
            var c = constructor.newInstance();
            c.alias = alias;
            return c;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new AliasConstructionException();
        }
    }

    @SuppressWarnings("unchecked")
    private Class<T> getSubClass() {
        return (Class<T>) getClass();
    }
}
