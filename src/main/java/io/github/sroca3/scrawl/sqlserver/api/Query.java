package io.github.sroca3.scrawl.sqlserver.api;

import io.github.sroca3.scrawl.sqlserver.schema.AbstractTable;
import io.github.sroca3.scrawl.sqlserver.statements.select.SelectColumnsClause;
import io.github.sroca3.scrawl.sqlserver.statements.common.TerminatingClause;
import io.github.sroca3.scrawl.sqlserver.statements.update.UpdateClause;
import io.github.sroca3.scrawl.sqlserver.schema.Column;
import io.github.sroca3.scrawl.sqlserver.schema.Expression;
import io.github.sroca3.scrawl.sqlserver.schema.SimpleColumn;
import io.github.sroca3.scrawl.sqlserver.statements.select.SelectQueryBuilder;
import io.github.sroca3.scrawl.sqlserver.statements.update.UpdateQueryBuilder;

public class Query {

    private Query() {
    }

    public static Column star() {
        return new SimpleColumn("*");
    }

    public static TerminatingClause selectOne() {
        return new TerminatingClause() {
            @Override
            public String getSql() {
                return "SELECT 1";
            }
        };
    }

    public static DistinctClause select() {
        return new DistinctClause();
    }

    public static SelectColumnsClause select(String... columns) {
        return new SelectQueryBuilder(columns);
    }

    public static SelectColumnsClause select(Column... columns) {
        return new SelectQueryBuilder(columns);
    }

    public static UpdateClause update(String table){
        var queryBuilder = new UpdateQueryBuilder();
        return queryBuilder.update(table);
    }

    public static <T extends AbstractTable<T>> UpdateClause update(AbstractTable<T> table){
        return update(table.getName());
    }

    public static Expression lhs(String lhs) {
        return new SimpleColumn(lhs);
    }

    public static class DistinctClause {
        public static SelectColumnsClause distinct(Column... columns) {
            final var builder = new SelectQueryBuilder(columns);
            builder.distinct();
            return builder;
        }
    }
}
