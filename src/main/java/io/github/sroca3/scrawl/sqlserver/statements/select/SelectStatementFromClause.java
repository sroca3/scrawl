package io.github.sroca3.scrawl.sqlserver.statements.select;

import io.github.sroca3.scrawl.sqlserver.statements.common.FromClause;

public interface SelectStatementFromClause extends FromClause, SelectTerminatingClause {
}
