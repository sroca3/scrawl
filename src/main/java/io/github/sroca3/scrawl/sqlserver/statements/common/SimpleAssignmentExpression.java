package io.github.sroca3.scrawl.sqlserver.statements.common;

public class SimpleAssignmentExpression implements AssignmentExpression {

    private final String lhs;
    private final Object rhs;
    private String parameterName;

    public SimpleAssignmentExpression(String lhs, Object rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public void generateParameters(Parameters parameters) {
        if (!String.valueOf(rhs).startsWith(":")) {
            parameterName = parameters.generateParameterName(lhs);
            parameters.addParameter(parameterName, rhs);
        }
    }

    @Override
    public String getSql() {
        if (parameterName != null) {
            return String.join(" ", lhs, "=", parameterName);
        } else {
            return String.join(" ", lhs, "=", String.valueOf(rhs));
        }
    }
}
