package io.github.sroca3.scrawl.sqlserver.statements.common;

import io.github.sroca3.scrawl.sqlserver.schema.Column;
import io.github.sroca3.scrawl.sqlserver.schema.FunctionColumn;

public class SqlFunction {

    private SqlFunction() {
    }

    public static Column count(Column column) {
        return new FunctionColumn(SqlServerFunction.COUNT, column);
    }
}
