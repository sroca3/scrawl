package io.github.sroca3.scrawl.sqlserver.statements.common;

import io.github.sroca3.scrawl.sqlserver.schema.Condition;
import io.github.sroca3.scrawl.sqlserver.statements.common.FromClause;

public interface JoinClause {
    FromClause on(Condition condition);
}
