package io.github.sroca3.scrawl.sqlserver.schema.condition;

import io.github.sroca3.scrawl.sqlserver.statements.common.Parameters;
import io.github.sroca3.scrawl.sqlserver.schema.Condition;

public abstract class AbstractCondition implements Condition {
    protected static final String SPACE = " ";
    private final Parameters parameters = new Parameters();

    @Override
    public Condition or(Condition condition) {
        return getCondition(condition, "OR");
    }

    @Override
    public Condition and(Condition condition) {
        return getCondition(condition, "AND");
    }

    private Condition getCondition(Condition condition, String logicalOperator) {
        if (condition instanceof CombiningCondition combiningCondition) {
            return new NestedCondition(this, logicalOperator, combiningCondition);
        }
        return new CombiningCondition(this, logicalOperator, condition);
    }
}
