package io.github.sroca3.scrawl.sqlserver;

import io.github.sroca3.scrawl.sqlserver.schema.AbstractTable;
import io.github.sroca3.scrawl.sqlserver.schema.Column;
import io.github.sroca3.scrawl.sqlserver.schema.Condition;
import io.github.sroca3.scrawl.sqlserver.schema.SimpleColumn;
import io.github.sroca3.scrawl.sqlserver.schema.Table;
import io.github.sroca3.scrawl.sqlserver.statements.common.AssignmentExpression;
import io.github.sroca3.scrawl.sqlserver.statements.common.JoinType;
import io.github.sroca3.scrawl.sqlserver.statements.common.Parameters;
import io.github.sroca3.scrawl.sqlserver.statements.select.SelectTerminatingClause;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class SqlBuilder {
    private static final String SPACE = " ";
    private static final String COMMA_SPACE = ", ";
    private static final String SELECT = "SELECT";
    private static final String DISTINCT = "DISTINCT";
    private static final String UPDATE = "UPDATE";
    private static final String FROM = "FROM";
    private static final String WHERE = "WHERE";
    private static final String ON = "ON";
    private static final String SET = "SET";

    private boolean distinct = false;
    private List<Column> columns = Collections.emptyList();
    private Table<?> rootTable;
    private String whereClause = "";
    private String orderByClause = "";
    private final Parameters parameters = new Parameters();
    private String joinClause = "";
    private String groupByClause = "";
    private String havingClause = "";
    private String setClause = "";
    private List<String> unionClauses = new LinkedList<>();

    public String build() {
        var builder = new StringBuilder();
        if (isSelect()) {
            builder.append(SELECT).append(SPACE);
            appendDistinct(builder);
            appendColumns(builder);
            appendRootTable(builder);
            appendJoinClause(builder);
            appendWhereClause(builder);
            appendGroupByClause(builder);
            appendHavingClause(builder);
            appendOrderByClause(builder);
            appendUnionClauses(builder);
        } else {
            builder.append(UPDATE);
            appendRootTable(builder);
            appendSetClause(builder);
            appendWhereClause(builder);
        }
        return builder.toString();
    }

    private void appendDistinct(StringBuilder builder) {
        if (distinct) {
            builder.append(DISTINCT).append(SPACE);
        }
    }

    private void appendUnionClauses(StringBuilder builder) {
        if (!unionClauses.isEmpty()) {
            builder.append(" UNION ");
            builder.append(String.join(" UNION ", unionClauses));
        }
    }

    private void appendSetClause(StringBuilder builder) {
        builder.append(SPACE).append(SET).append(SPACE).append(setClause);
    }

    private boolean isSelect() {
        return !this.columns.isEmpty();
    }

    private void appendHavingClause(StringBuilder builder) {
        builder.append(this.havingClause);
    }

    private void appendGroupByClause(StringBuilder builder) {
        if (isNotBlank(this.groupByClause)) {
            builder.append(this.groupByClause);
        }
    }

    private void appendJoinClause(StringBuilder builder) {
        if (isNotBlank(this.joinClause)) {
            builder.append(joinClause);
        }
    }

    private void appendOrderByClause(StringBuilder builder) {
        if (isNotBlank(this.orderByClause)) {
            builder.append(this.orderByClause);
        }
    }

    private void appendColumns(StringBuilder builder) {
        builder.append(columns.parallelStream().map(Column::getName).collect(Collectors.joining(COMMA_SPACE)));

    }

    private void appendRootTable(StringBuilder builder) {
        if (rootTable != null) {
            if (isSelect()) {
                builder.append(SPACE)
                    .append(FROM);
            }
            builder.append(SPACE)
                .append(rootTable.getName());
            if (isNotBlank(rootTable.getAlias())) {
                builder.append(SPACE)
                    .append(rootTable.getAlias());
            }
        }
    }

    private void appendWhereClause(StringBuilder builder) {
        if (isNotBlank(whereClause)) {
            builder.append(SPACE)
                .append(WHERE)
                .append(SPACE)
                .append(whereClause);
        }
    }

    private boolean isNotBlank(String string) {
        return string != null && !string.isBlank();
    }

    public void addColumnNames(List<String> columns) {
        this.columns = columns.parallelStream().map(SimpleColumn::new).collect(toList());
    }

    public void addRootTable(String tableName) {
        this.rootTable = new GenericTable(tableName);
    }

    public void addRootTable(Table<?> table) {
        this.rootTable = table;
    }

    public void addColumns(List<Column> columns) {
        this.columns = List.copyOf(columns);
    }

    public void addConditionToWhereClause(Condition condition) {
        condition.generateParameters(this.parameters);
        this.whereClause = condition.getSql();
    }

    public void addOrderByClause(String[] columns) {
        this.orderByClause = " ORDER BY " + String.join(COMMA_SPACE, columns);
    }

    public Map<String, Object> getParameterMap() {
        return parameters.toMap();
    }

    public void addJoinClause(JoinType joinType, Table<?> table) {
        this.joinClause = this.joinClause + SPACE + joinType.getKeyword() + SPACE + table.getName();
    }

    public void addJoinCondition(Condition condition) {
        this.joinClause = this.joinClause + SPACE + ON + SPACE + condition.getSql();
    }

    public void addGroupByClause(Column column) {
        this.groupByClause = " GROUP BY " + column.getName();
    }

    public void addHavingClause(Condition condition) {
        this.havingClause = " HAVING " + condition.getSql();
    }

    public void addOrderByClause(Column[] columns) {
        this.addOrderByClause(Arrays.stream(columns)
            .map(Column::getName)
            .toList()
            .toArray(new String[0]));
    }

    public void update(String table) {
        this.addRootTable(table);
    }

    public void addSetClause(AssignmentExpression[] assignments) {
        Arrays.stream(assignments)
            .forEach(assignmentExpression -> assignmentExpression.generateParameters(this.parameters));
        this.setClause = Arrays.stream(assignments)
            .map(AssignmentExpression::getSql)
            .collect(Collectors.joining(","));
    }

    public void union(SelectTerminatingClause selectTerminatingClause) {
        unionClauses.add(selectTerminatingClause.getSql());
    }

    public void addDistinct() {
        this.distinct = true;
    }

    private static class GenericTable extends AbstractTable<GenericTable> {
        public GenericTable(String tableName) {
            super(tableName);
        }
    }
}
