package io.github.sroca3.scrawl.sqlserver.schema.condition;

import io.github.sroca3.scrawl.sqlserver.statements.common.Parameters;
import io.github.sroca3.scrawl.sqlserver.schema.Condition;
import io.github.sroca3.scrawl.sqlserver.schema.Operator;

public class SimpleCondition extends AbstractCondition implements Condition {
    private String lhs;
    private String operator;
    private Object parameter;
    private String parameterName;

    public SimpleCondition(String lhs, String operator, Object parameter) {
        this.lhs = lhs;
        this.operator = operator;
        this.parameter = parameter;
    }

    @Override
    public void generateParameters(Parameters parameters) {
        if (!String.valueOf(this.parameter).startsWith(":")) {
            parameterName = parameters.generateParameterName(this.lhs);
            parameters.addParameter(parameterName, this.parameter);
        }
    }

    @Override
    public String getSql() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.lhs)
               .append(SPACE)
               .append(this.operator)
               .append(SPACE);
        if (Operator.IN.getOperator().equals(operator)) {
            builder.append("(");
        }
        if (parameterName != null) {
            builder.append(parameterName);
        } else {
            builder.append(this.parameter);
        }
        if (Operator.IN.getOperator().equals(operator)) {
            builder.append(")");
        }
        return builder.toString();
    }
}
