package io.github.sroca3.scrawl.sqlserver.statements.update;

import io.github.sroca3.scrawl.sqlserver.*;
import io.github.sroca3.scrawl.sqlserver.schema.Column;
import io.github.sroca3.scrawl.sqlserver.schema.Condition;
import io.github.sroca3.scrawl.sqlserver.schema.Table;
import io.github.sroca3.scrawl.sqlserver.statements.common.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class UpdateQueryBuilder implements UpdateClause, SetClause, TerminatingClause {

    private final SqlBuilder sqlBuilder;

    public UpdateQueryBuilder() {
        this.sqlBuilder = new SqlBuilder();
    }

    @Override
    public TerminatingClause where(Condition condition) {
        sqlBuilder.addConditionToWhereClause(condition);
        return this;
    }

    @Override
    public String getSql() {
        return sqlBuilder.build();
    }

    @Override
    public Map<String, Object> getParameterMap() {
        return sqlBuilder.getParameterMap();
    }

    public UpdateClause update(String table) {
        sqlBuilder.update(table);
        return this;
    }

    @Override
    public SetClause set(AssignmentExpression... assignmentExpressions) {
        sqlBuilder.addSetClause(assignmentExpressions);
        return this;
    }
}
