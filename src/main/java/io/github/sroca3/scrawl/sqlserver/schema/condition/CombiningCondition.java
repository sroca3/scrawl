package io.github.sroca3.scrawl.sqlserver.schema.condition;

import io.github.sroca3.scrawl.sqlserver.statements.common.Parameters;
import io.github.sroca3.scrawl.sqlserver.schema.Condition;

public class CombiningCondition extends AbstractCondition implements Condition {
    protected final Condition leftCondition;
    protected final String logicalOperator;
    protected final Condition rightCondition;

    public CombiningCondition(Condition leftCondition, String logicalOperator, Condition rightCondition) {
        this.leftCondition = leftCondition;
        this.logicalOperator = logicalOperator;
        this.rightCondition = rightCondition;
    }

    @Override
    public void generateParameters(Parameters parameters) {
        this.leftCondition.generateParameters(parameters);
        this.rightCondition.generateParameters(parameters);
    }

    @Override
    public String getSql() {
        String sql = this.leftCondition.getSql();
        sql += SPACE + logicalOperator + SPACE;
        sql += this.rightCondition.getSql();
        return sql;
    }
}
